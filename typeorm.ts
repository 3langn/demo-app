import express from "express";
import { createConnection, getConnection } from "typeorm";
import { Notification } from "./src/entity/Notification";
import { Post } from "./src/entity/Post";

const app = express();
createConnection().then(async () => {
  app.get("/", async (req, res) => {
    const connection = getConnection();
    const queryRunner = connection.createQueryRunner();

    await queryRunner.connect();
    const post = Post.create({
      title: "Test",
      description: "Test description11",
    });
    const notification = Notification.create({
      title: "Test",
      description: "Test description11",
    });

    try {
      await queryRunner.startTransaction();
      await queryRunner.manager.save(post);
      await queryRunner.manager.save(notification);

      //Khong luu vao db khi khong tim thay
      await Notification.findOneOrFail({ where: { id: "asgdjahgsg" } });
    } catch (error) {
      console.log(error);

      await queryRunner.rollbackTransaction();
    } finally {
      await queryRunner.release();
    }
    res.send();
  });
  app.listen(3000, () => {
    console.log("listening on 3000");
  });
});

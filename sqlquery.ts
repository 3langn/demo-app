import mysql from "mysql2/promise";
const pool = mysql.createPool({
  host: "localhost",
  user: "root",
  password: "",
  database: "transaction",
});
pool.getConnection().then(async (connection) => {
  try {
    await connection.query(
      "CREATE TABLE IF NOT EXISTS posts (title VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL)"
    );
    await connection.query("START TRANSACTION");

    await connection.query(
      "INSERT INTO posts (title, description) VALUES(?,?)",
      ["Hiiiii", "AAAAAAAAA"]
    );

    await connection.query(
      "INSERT INTO notifications (title, description) VALUES(?,?)",
      ["Hiiiii", "BBBBBBBBBBB"]
    );
    await connection.query("COMMIT");
  } catch (e) {
    console.log(e);
    await connection.query("ROLLBACK");
  }
});

# Demo App

### Start

- Tạo database transaction và transactions :v
- Chỉnh lại script start `ts-node-dev sqlquery.ts hoặc typeorm.ts ` trong package.json

### Database Transaction

- Trong database, một Transaction là tập hợp các thao tác mà tất cả các thao tác phải thực hiện thành công, hoặc là không thao tác nào thành công.
- ACID(Atomicity, Consistency, Isolation, Durability ) định nghĩa các tính chất của một transaction trong database.
  - Atomicity: tính chất này thể hiện khái niệm “hoặc là tất cả hoặc là không gì hết”. Toàn bộ các bước được thực hiện trong một transaction nếu thành công thì phải thành công tất cả, nếu thất bại thì tất cả cũng phải thất bại. Nếu một transaction thành công thì tất cả những thay đổi phải được lưu vào database. Nếu thất bại thì tất cả những thay đổi trong transaction đó phải được rollback về trạng thái ban đầu.
  - Consistency: dữ liệu từ thời điểm start transaction với lúc kết thúc phải nhất quán.
  - Isolation: nếu chúng ta có nhiều transaction cùng một lúc thì phải đảm bảo là các transaction này xảy ra độc lập, không tác động lẫn nhau trên dữ liệu.
  - Durability: điều này có nghĩa dữ liệu sau khi thực hiện transaction sẽ không thay đổi nếu chúng ta gặp vấn đề gì đó liên quan đến database.

#### Isolation Level of MySQL

Giả sử khi chúng ta đang tiến hành song song và đồng thời 2 transaction cùng cập nhật giá trị vào 1 bản ghi trong CSDL. Ở đây sẽ xảy ra concurency giữa các transaction và xảy ra các vấn đề :

Transaction trước hay sau sẽ được tiến hành hay cả 2 cùng được tiến hành một lúc.
Kết quả cuối cùng là kết quả của transaction nào trước hay sau? Ở đây xảy ra concurency giữa các transaction

- Read Uncommitted
  Một transaction lấy dữ liệu từ một transaction khác ngay cả khi transaction đó chưa được commit
- Read Committed
  Đây là level default của một transaction nếu như chúng ta không config gì thêm. Tại level này thì Transaction sẽ không thể đọc dữ liệu từ từ một Transaction đang trong quá trình cập nhật hay sửa đổi mà phải đợi transacction đó hoàn tất
- Repeatable read
  Giống như mức độ của Read Committed, tại mức độ này thì transaction còn không thể đọc / ghi đè dữ liệu từ một transaction đang tiến hành cập nhật trên bản ghi đó
- Serializable
  Level cao nhất của Isolation, khi transaction tiến hành thực thi nó sẽ khóa các bản ghi liên quan và sẽ unlock cho tới khi rollback hoặc commit dữ liệu
- SnapShot
  Tương tự với level Serializable, nhưng cách thức hoạt động nó lại khác so với Serializable. Khi một transaction select các bản ghi thì nó sẽ không lock các bản ghi này lại, mà tạo một bản sao trên bản ghi hoặc các bản ghi đó. Khi ta tiến hành UPDATE / DELETE ta tiến hành trên bản sao dữ liệu đó và không gây ảnh hưởng tới dữ liệu ban đầu. Ưu điểm của snapshot là giảm độ trễ giữa các transaction nhưng bù lại cần tốn thêm tài nguyên lưu trữ các bản sao.

https://topdev.vn/blog/khai-niem-transaction-trong-database/

https://viblo.asia/p/isolation-level-of-mysql-63vKjRmAK2R

#### Transaction trong TypeORM

https://github.com/typeorm/typeorm/blob/master/docs/transactions.md
